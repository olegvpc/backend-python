import cv2
import threading
import time

src = 'rtsp://admin:@188.162.85.173:561/user=admin_password=tlJwpbo6_channel=1_stream=0.sdp?real_stream'
capture = cv2.VideoCapture(src)
codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')


frame_width = int(capture.get(3))
frame_height = int(capture.get(4))

print('frame_width', frame_width)
print('frame_height', frame_height)

output_video = cv2.VideoWriter(
    'out.avi',
    codec,
    15,
    (frame_width, frame_height)
)


t = time.time()
while(True):

    ret, frame = capture.read()
    output_video.write(frame)
    if time.time() - t > 60:
        break

cap.release()
out.release()
cv2.destroyAllWindows()

# def write_to_file():
#     while True:
#         if capture.isOpened():
#             (_, frame) = capture.read()
#             output_video.write(frame)
#             del frame
#
# thread = threading.Thread(target=write_to_file, args=())
# thread.start()