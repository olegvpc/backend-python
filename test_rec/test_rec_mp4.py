import cv2, sys
import threading
import time

src = 'rtsp://admin:@188.162.85.173:561/user=admin_password=tlJwpbo6_channel=1_stream=0.sdp?real_stream'
cap = cv2.VideoCapture(src)

codec = cv2.VideoWriter_fourcc(*'mp4v')

frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
frame_size = (frame_width, frame_height)
print("Video size: ", frame_size)

out = cv2.VideoWriter('out.mp4', codec, 15.0, frame_size)

t = time.time()
while(True):

    ret, frame = cap.read()
    out.write(frame)
    if time.time() - t > 60:
        break

cap.release()
out.release()
cv2.destroyAllWindows()