#!/usr/bin/env python
import os
import multiprocessing
from include.redis import redis_conn
from include.config import WorkerConfig
from rq import Queue, Worker
from dotenv import load_dotenv


load_dotenv()
# создпется экземпляр из класса конфигурации
config = WorkerConfig()
# объект с конфигурацией камеры получен c API https://backend-api.revisorvision.ru/api/workers/get-configurations?worker=worker77
cameras_config = config.get_config( os.getenv('WORKER_ID') )

workers = []
# создание очереди и worker для потоковой обоработки задач
for key in cameras_config:
    queue = Queue('concatenate_video_' + os.getenv('WORKER_ID') + '_' + cameras_config[key]['camera_id'], connection=redis_conn)
    worker = Worker(queue, connection=redis_conn)

    p = multiprocessing.Process(target=worker.work)
    workers.append(p)
    p.start()