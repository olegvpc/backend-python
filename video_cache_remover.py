from os import walk
from pathlib import Path
from dotenv import load_dotenv
import datetime as dt
import re
import os
import sys


load_dotenv()
MIN_VIDEO_AGE = 2


def get_dir_items(path, is_folders = True):
    return next(walk(path), (None, None, []))[1 if is_folders else 2]

def remove_file(filepath):
    try:
        if os.path.isfile(filepath):
            print("Удаление видео файла '{}'".format(filepath))
            os.remove(filepath)
        else:
            print("Ошибка удаления видео файла '{}'. Файла не существует".format(filepath))

        if os.path.isfile(filepath):
            print("Ошибка удаления видео файла '{}'. Файл не удален!".format(filepath))
    except:
        print("Ошибка удаления видео файла '{}'. Файл не удален!".format(filepath))

def get_vidoe_output_list_to_remove():
    root_path = "/app/media/video_output"
    files_to_remove = []

    videos_list = get_dir_items(root_path, False)
    for video_file in videos_list:
        result = re.findall(r'\d{1,2}_\d{1,2}_\d{4}', video_file) # ex: 12_05_2022_18_00_35.mp4
        date = result[0] if len(result) > 0 else False
        if not date:
            print("Дата видео файла не определена")
            continue
        # Парсинг даты видео файла
        date = dt.datetime.strptime(date, '%d_%m_%Y')
        date = date.date()
        print("Видео файл:", video_file, "Дата видео файла:", date)

        video_age = (dt.datetime.today().date() - date).days
        print("Возрост видео файла (в днях):", video_age)
        if video_age < MIN_VIDEO_AGE:
            print("Файл слшком рано удалять. Должно пройти минимум (дней):", MIN_VIDEO_AGE)
            continue

        files_to_remove.append({
            "path": root_path + os.sep + video_file
        })
    return files_to_remove

def get_segments_list_to_remove():
    root_path = "/app/media/segments/"
    files_to_remove = []
    cameras_folders = get_dir_items(root_path, True)
    for cameras_folder in cameras_folders:
        camera_id = cameras_folder[7:]
        print("======= Идентификатор камеры:", camera_id, "=======")

        videos_list = get_dir_items(root_path + cameras_folder, False)
        for video_file in videos_list:
            result = re.findall(r'\d{1,2}_\d{1,2}_\d{4}', video_file) # ex: 12_05_2022_18_00_35.mp4
            date = result[0] if len(result) > 0 else False
            if not date:
                print("Дата видео файла не определена")
                continue
            # Парсинг даты видео файла
            date = dt.datetime.strptime(date, '%d_%m_%Y')
            date = date.date()
            print("Видео файл:", video_file, "Дата видео файла:", date)

            video_age = (dt.datetime.today().date() - date).days
            print("Возрост видео файла (в днях):", video_age)
            if video_age < MIN_VIDEO_AGE:
                print("Файл слшком рано удалять. Должно пройти минимум (дней):", MIN_VIDEO_AGE)
                continue

            files_to_remove.append({
                "path": root_path + cameras_folder + os.sep + video_file
            })
    return files_to_remove


# Старт удаления видео файлов
files_to_remove = get_segments_list_to_remove()
files_to_remove2 = get_vidoe_output_list_to_remove()

for file_to_remove in files_to_remove:
    remove_file(file_to_remove['path'])

for file_to_remove in files_to_remove2:
    remove_file(file_to_remove['path'])