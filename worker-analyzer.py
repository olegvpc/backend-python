#!/usr/bin/env python
import os
import multiprocessing
from include.redis import redis_conn
from include.config import WorkerConfig
from rq import Queue, Worker
from dotenv import load_dotenv


load_dotenv()

config = WorkerConfig()
cameras_config = config.get_config( os.getenv('WORKER_ID') )

workers = []
for key in cameras_config:
    queue = Queue('analyze_video_' + os.getenv('WORKER_ID') + '_' + cameras_config[key]['camera_id'], connection=redis_conn)
    worker = Worker(queue, connection=redis_conn)

    p = multiprocessing.Process(target=worker.work)
    workers.append(p)
    p.start()