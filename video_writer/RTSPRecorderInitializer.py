from threading import Timer
from video_writer.RTSPRecorder import RTSPRecorder
from datetime import datetime
from include.logger import getLogger
from include.monitoring import Monitoring

logger = getLogger('video-recorder')
monitoring = Monitoring()


class RTSPRecorderInitializer(object):

    RETRY_CHECK_TIME = 60.0
    start_or_stop_timer = False
    start: datetime
    end: datetime

    """
    Конструктор класса RTSPRecorderInitializer
    """
    def __init__(self, camera_id, src, fps, recording_period_start, recording_period_end, segment_duration):
        self.camera_id = camera_id
        self.src = src
        self.fps = fps
        self.recording_period_start = recording_period_start
        self.recording_period_end = recording_period_end

        self.segment_duration = segment_duration
        if not self.segment_duration:
            self.segment_duration = 60

        self.rtsp_recorder = RTSPRecorder(
            self.camera_id,
            self.src,
            self.fps,
            self.recording_period_start,
            self.recording_period_end,
            self.segment_duration
        )

        self.create_or_destroy_video_writer()

    """
    Попытка инициализации объекта записывающего видео поток в файл
    """
    def create_or_destroy_video_writer(self):
        try:
            now = datetime.now()
            monitoring.set(self.camera_id, 'start-stop-video-recording', now.strftime("%Y-%m-%d %H:%M:%S"))
            start_parsed = self.recording_period_start.split(':')
            self.start = now.replace(hour=int(start_parsed[0]), minute=int(start_parsed[1]), second=0, microsecond=0)
            end_parsed = self.recording_period_end.split(':')
            self.end = now.replace(hour=int(end_parsed[0]), minute=int(end_parsed[1]), second=0, microsecond=0)

            logger.logger.debug("Проверка запуска или остановки записи видео")
            logger.logger.debug("Период записи видео потока c " + self.start.strftime("%H:%M") + " по " + self.end.strftime("%H:%M"))
            logger.logger.debug("Текущее время: " + now.strftime("%H:%M"))

            if self.start <= now <= self.end:
                # Запись должна быть запущена
                if not self.rtsp_recorder.is_write_to_file_working():
                    logger.logger.debug("Попытка открыть видео поток с камеры #" + self.camera_id)
                    self.rtsp_recorder.start()
                    logger.logger.debug("Статус открытия видео потока:" + "Открыт" if self.rtsp_recorder.is_opened() else "Закрыт")
                    if not self.rtsp_recorder.is_opened():
                        logger.logger.debug("Ошибка при открытии видео. Следующая попытка открытия видео потока будет через 1 минуту")
                else:
                    logger.logger.debug("Идет запись видео с камеры #" + self.camera_id)

                    # Проверка что таймер который пересоздает видео файлы живой
                    if not self.rtsp_recorder.is_timer_working():
                        logger.logger.debug(f'Таймер пересоздания сегментов на камере {self.camera_id} не работает! Попытка пересоздать сегмент')
                        self.rtsp_recorder.recreate_video_writer()
            else:
                # Запись должна быть остановлена
                if self.rtsp_recorder.is_opened():
                    logger.logger.debug("Попытка остановки записи видео потока для камеры #" + self.camera_id)
                    self.rtsp_recorder.stop(external=True)
        finally:
            self.start_or_stop_timer = Timer(self.RETRY_CHECK_TIME - int(datetime.now().strftime("%S")), self.create_or_destroy_video_writer)
            self.start_or_stop_timer.start()
        return
    def restart(self, camera_id, src, fps, recording_period_start, recording_period_end, segment_duration):
        if self.start_or_stop_timer:
            self.start_or_stop_timer.cancel()
        self.rtsp_recorder.stop(external=True)
        self.__init__(camera_id, src, fps, recording_period_start, recording_period_end, segment_duration)
