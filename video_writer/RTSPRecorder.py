from pathlib import Path
from datetime import datetime
from include.redis_producer import add_video_for_analyze
from include.logger import getLogger
import cv2
import threading
import os
from include.monitoring import Monitoring
from include.video_utils import get_video_frames
from include.video_utils import remove_file
import numpy as np

logger = getLogger('video-recorder')
monitoring = Monitoring()


class RTSPRecorder(object):
    DEFAULT_FPS = 15
    OPENED_STREAM_READ_ERRORS_LIMIT = 150
    now: datetime
    fps: int
    segment_duration: int
    src: str
    camera_id: str
    recording_period_start: str
    recording_period_end: str
    write_to_file_thread: threading.Thread = False
    recreate_timer: threading.Timer = False
    output_video = False
    video_file_path: str = ""
    capture: cv2.VideoCapture = False
    frame_width: int
    frame_height: int
    codec: cv2.VideoWriter_fourcc

    """
    Конструктор класса RTSPRecorder
    """

    def __init__(self, camera_id, src, fps, recording_period_start, recording_period_end, segment_duration):
        # Сохраняем параметры в св-ах класса
        self.camera_id = camera_id
        self.src = src
        self.fps = fps
        self.recording_period_start = recording_period_start
        self.recording_period_start_parsed = self.recording_period_start.split(':')
        self.recording_period_end = recording_period_end
        self.recording_period_end_parsed = self.recording_period_end.split(':')
        self.segment_duration = segment_duration

        logger.logger.debug("=============== Создан объект RTSPRecorder =============")
        logger.logger.debug("Идентификатор камеры: " + self.camera_id)
        logger.logger.debug("Источник: " + self.src)
        logger.logger.debug("FPS: " + str(self.fps))
        logger.logger.debug("Segment duration: " + str(self.segment_duration))

        # Set up codec and output video settings
        self.codec = cv2.VideoWriter_fourcc(*'mp4v')

        # Lock для приостановки чтения/записи кадров на момент обновления потоков
        self.global_rw_lock = threading.Lock()

    """
    Открыт ли видеопоток или нет
    """

    def is_opened(self):
        if not self.capture:
            value = False
        else:
            value = self.capture.isOpened()
        monitoring.set(self.camera_id, 'video-recording-status', 1 if value else 0)
        return value

    """
    Открытие видео потока и старт записи
    """

    def start(self):
        self.frames_read = 0
        self.success_frames_read = 0
        self.opened_stream_read_errors = 0
        self.external_stop = False

        # Create a VideoCapture object
        logger.logger.debug("Открытие видео потока для камеры #" + self.camera_id)
        self.capture = cv2.VideoCapture(self.src)

        # Проверяем что поток открылся
        if not self.capture.isOpened():
            logger.logger.debug("Ошибка открытия видео потока для камеры #" + self.camera_id)
            monitoring.set(self.camera_id, 'current-stream-status', 'Нет потока')
            return
        else:
            monitoring.set(self.camera_id, 'current-stream-status', 'Поток открыт')

        logger.logger.debug("Видео поток открыт для камеры #" + self.camera_id)

        # Default resolutions of the frame are obtained (system dependent)
        self.frame_width = int(self.capture.get(3))
        self.frame_height = int(self.capture.get(4))
        logger.logger.debug("Разрешение кадра в видео потоке: " + str(self.frame_width) + "x" + str(self.frame_height))

        # Create folder for storage videos
        self.create_folder()

        # Create Writer
        if self.recreate_timer:
            if not self.is_timer_working():
                self.recreate_video_writer()
        else:
            self.recreate_video_writer()

        # Start Recording
        self.write_to_file_thread = threading.Thread(target=self.write_to_file, args=())
        self.write_to_file_thread.start()
        logger.logger.debug("Запись видео потока в файл запущена")

    """
    Закрытие видео потока и остановка записи
    """

    def stop(self,noblock=False,external=False):
        if external:
            self.external_stop = True
        self.frames_read = 0
        self.success_frames_read = 0
        self.opened_stream_read_errors = 0

        logger.logger.debug("Остановка записи видео потока для камеры #" + self.camera_id)
        if self.recreate_timer:
            self.recreate_timer.cancel()
            logger.logger.debug("recreate_timer остановлен")

        if self.video_file_path != "":
            if not noblock:
                self.global_rw_lock.acquire()
            if self.output_video:
                if self.output_video.isOpened():
                    self.output_video.release()

            # Освобождаем видео поток
            logger.logger.debug("Освобождение видеопотока для камеры #" + self.camera_id)
            if self.capture:
                if self.capture.isOpened():
                    self.capture.release()
                    #self.capture = None
                logger.logger.debug("Видеопоток освобожден для камеры #" + self.camera_id)
            if not noblock:
                self.global_rw_lock.release()

            # Отправка события на обработку в Redis
            self.send_event_to_redis(self.video_file_path,self.now)
            logger.logger.debug("Сообщение отправлено в Redis")
            self.video_file_path = ""


    def is_timer_working(self):
        """
        Проверка что таймер который пересоздает видео файлы живой
        :return:
        """
        return self.recreate_timer.is_alive()

    def is_write_to_file_working(self):
        if not self.write_to_file_thread:
            return False
        else:
            return self.write_to_file_thread.is_alive()

    """
    Пересоздание отрезка с данными из видео потока
    """

    def recreate_video_writer(self):
        if self.recreate_timer:
            self.recreate_timer.cancel()


        # Выносим текущие значения в отдельные переменные для отправки события в redis
        # после обновления сегмента, чтобы не блокировать запись во время отправки
        if self.video_file_path != "":
            old_video_path = self.video_file_path
            old_date = self.now
        else:
            old_video_path = None
            old_date = None


        self.video_file_path = '/app/media/segments/camera_' + self.camera_id + '/' + self.generate_file_name()
        monitoring.set(self.camera_id, 'current-video-segment-path', self.video_file_path)
        logger.logger.debug("Запись в файл:" + self.video_file_path)

        # Создаем новый сегмент вне блокировки записи
        new_video_writer = cv2.VideoWriter(
            self.video_file_path,
            self.codec,
            self.fps if self.fps else self.DEFAULT_FPS,
            (self.frame_width, self.frame_height)
        )

        # Блокируем запись видео в момент пересоздания сегмента
        with self.global_rw_lock:
            if self.output_video:
                if self.output_video.isOpened():
                    self.output_video.release()

            # Во время блокировки просто подменяем self.output_video выше созданным сегментом
            self.output_video = new_video_writer

        # Отправка события на обработку в Redis
        if all((old_video_path,old_date)):
            self.send_event_to_redis(old_video_path, old_date)


        # Пересоздадим файл через %segment_duration% секунд
        # Значение self.segment_duration задается через базу данных
        self.recreate_timer = threading.Timer(self.segment_duration, self.recreate_video_writer)
        self.recreate_timer.start()

    """
    Отправляем событие для дальнейшего анализа фрагмента видео в Redis
    """

    def send_event_to_redis(self,old_video_path,old_date):
        if not os.path.isfile(old_video_path):
            return
        frames = get_video_frames(old_video_path)
        if frames == 0:
            logger.logger.debug(f'Камера {self.camera_id}: в сегменте {old_video_path} 0 кадров')
            #remove_file(old_video_path)
        else:
            add_video_for_analyze(self.camera_id, old_video_path, old_date)
            logger.logger.debug("(Camera ID: {}) Событие на обработку видео отправлено в Redis".format(self.camera_id))

    """
    Запись данных в файл
    """

    def write_to_file(self):
        while True:
            self.frames_read += 1
            with self.global_rw_lock:
                if self.capture.isOpened():
                    success, frame = self.capture.read()
                    if success:
                        #frame_mean = frame[::10, ::10, :].mean((0, 1))
                        #mean_delta = max(frame_mean) - min(frame_mean)
                        unique_colors = len(np.unique(frame[::20, ::20, 0]))
                        if unique_colors > 32:
                            self.success_frames_read += 1
                            if self.output_video.isOpened():
                                self.output_video.write(frame)
                            else:
                                logger.logger.debug(f'Камера {self.camera_id}: попытка записи кадра в закрытый поток')
                        else:
                            logger.logger.debug(f'Камера {self.camera_id}: кадр {self.frames_read} прочитан успешно, но слишком мало уникальных цветов: {unique_colors}')
                    else:
                        self.opened_stream_read_errors+=1
                        logger.logger.debug(f'Камера {self.camera_id}: поток открыт, но произошла ошибка при чтении кадра')
                        if self.opened_stream_read_errors >= self.OPENED_STREAM_READ_ERRORS_LIMIT:
                            logger.logger.debug("Превышен лимит ошибок чтения при открытом потоке для камеры #" + self.camera_id+" Закрытие потока")
                            self.stop(noblock=True) # Не блокируем, чтобы не вызвать deadlock
                            monitoring.set(self.camera_id, 'current-stream-status', 'Закрыт программой (превышен лимит ошибок чтения при открытом потоке)')
                            break
                else:
                    if self.external_stop:
                        monitoring.set(self.camera_id, 'current-stream-status', 'Закрыт программой (вне периода записи)') # Исправить
                        break
                    else:
                        logger.logger.debug(f'Камера {self.camera_id}: попытка чтения кадра при закрытом потоке')

    """
    Создание директории для хранения файлов с отрезками видео потоков
    """

    def create_folder(self):
        # Create folder for storage videos
        Path("/app/media/segments/camera_" + self.camera_id).mkdir(parents=True, exist_ok=True)
        Path("/app/media/full/camera_" + self.camera_id).mkdir(parents=True, exist_ok=True)

    """
    Генерация имени файла для храниения отрезка видео потока
    """

    def generate_file_name(self):
        self.now = datetime.now()
        return self.now.strftime("%d_%m_%Y_%H_%M_%S") + ".mp4"
