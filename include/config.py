from include.logger import getLogger
from dotenv import load_dotenv
import requests
import os

load_dotenv()
logger = getLogger()
workerConfiguration = False

class WorkerConfig:
    """Класс создания рабочей конфигурации - возвращвет обьект-словарь с данными из API"""
    def get_config(self, worker_name, force=False):
        """Метод с запросом на API за конфигурацией"""
        global workerConfiguration

        if not workerConfiguration or force:
            workerConfiguration = {}
            r = requests.get(os.getenv('MAIN_REST_API_URL')+'/api/workers/get-configurations?worker='+worker_name)
            response = r.json()
            for item in response:
                workerConfiguration[ item['camera_id'] ] = item

        return workerConfiguration
