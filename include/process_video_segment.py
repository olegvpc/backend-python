import os.path
import os
import subprocess
import uuid
import datetime as dt
import re
import time
from shutil import copyfile
from pathlib import Path
from include.models_downloader import load_models
from include.logger import getLogger
from include.redis import redis_conn
from rq import Queue
from dotenv import load_dotenv
from include.monitoring import Monitoring
from include.video_utils import get_video_seconds
from include.video_utils import remove_file


load_dotenv()
logger = getLogger('model-executor')
logger_video_concat = getLogger('video-concat')
models = load_models()
monitoring = Monitoring()

FINAL_VIDEO_SCALE = '800:-1'
MIN_VIDEO_FILE_SIZE_IN_BYTES = 1000000 # 1 Mb

def process_video_segment(camera_id, filepath, from_time, camera_models, camera_markups, camera_start_recording_time, camera_fps):
    monitoring.set(camera_id, 'process-video-segment', filepath)

    """
    Метод для обработки видео отрезка
    """
    logger.logger.debug("(Camera #" + camera_id + "): Запускаем обработку файла для камеры #" + camera_id)
    logger.logger.debug("(Camera #" + camera_id + "): Путь до видео отрезка:" + filepath)
    logger.logger.debug("(Camera #" + camera_id + "): Дата/Время начала записи:" + from_time)

    # проверяем что файл существует
    if not os.path.isfile(filepath):
        logger.logger.exception(
            "(Camera #" + camera_id + '): Остановка обработки. Видео файл не сущетсвует по пути:' + filepath)
        return

    # проверяем что файл не пустой
    if os.path.getsize(filepath) == 0:
        logger.logger.exception(
            "(Camera #" + camera_id + '): Остановка обработки. Видео файл пустой по пути:' + filepath)
        return

    # парсим дату видео
    result = re.findall(r'\d{1,2}_\d{1,2}_\d{4}_\d{1,2}_\d{1,2}_\d{1,2}', os.path.basename(filepath))
    date = dt.datetime.strptime(result[0], '%d_%m_%Y_%H_%M_%S')

    # Обрабатываем видео отрезок моделями
    filepaths_with_markups = {}
    logger.logger.debug("(Camera #" + camera_id + "): Запускаем обработку видео отрезка моделями")
    for key in camera_models:
        logger.logger.debug("(Camera #" + camera_id + "): Обработка моделью:" + key)
        if key not in models:
            logger.logger.exception("(Camera #" + camera_id + "): Модель " + key + " не найдена")
            continue
        if key not in camera_markups:
            logger.logger.exception("(Camera #" + camera_id + "): Разметка " + key + " не найдена")
            continue

        # Определение пути хранения выходного видео
        video_input_name = os.path.basename(filepath)
        video_name_short = video_input_name.split('.')[0]
        video_output_name = '{}_{}_{}_out.mp4'.format(video_name_short, camera_id, key)
        output_path = os.path.join('/app/media/video_output', video_output_name)
        filepaths_with_markups[key] = output_path
        logger.logger.debug(
            "(Camera #" + camera_id + "): Выходной видео файл с созданой моделью разметкой:" + output_path)

        # Запуск обработкой моделью
        full_video_seconds = str(get_video_seconds(get_full_video_path(camera_id, key, date), camera_fps))
        logger.logger.debug("(Camera #{}): Запуск обработки видео отрезка моделью: {}".format(camera_id, key))
        logger.logger.debug(
            "(Camera #{}): Параметры запуска: filepath = {}, output_path = {}, markup = {}, camera_start_recording_time = {}, full_video_seconds = {}".format(
                camera_id,
                filepath,
                output_path,
                camera_markups[key],
                camera_start_recording_time,
                full_video_seconds
            )
        )
        models[key].apply(
            filepath,
            output_path,
            camera_id,
            camera_markups[key],
            camera_start_recording_time,
            full_video_seconds,
        )

    for key in filepaths_with_markups:
        # проверяем что выходной файл существует и в нем есть данные
        if os.path.isfile(filepaths_with_markups[key]) and os.path.getsize(
                filepaths_with_markups[key]) > MIN_VIDEO_FILE_SIZE_IN_BYTES:
            # Дополняем полное видео выходным файлом из модели
            save_full_video(key, filepaths_with_markups[key], date, camera_id, filepath)
        else:
            # Заглушка при подении модели. Если модель не создала видео, то используем оригинальное видео для дозаписи отрезка
            save_full_video(key, filepath, date, camera_id, filepath)



def get_full_video_path(camera_id, model_name, date):
    full_video_directory = "/app/media/full/camera_" + camera_id + "/"
    return full_video_directory + "full_video_" + model_name + "_" + date.strftime(
        "%d_%m_%Y") + ".mp4"


def compress_video(video_path):
    try:
        logger_video_concat.logger.debug('Сжатие видео файла: ' + video_path)
        compressed_video_path = os.path.splitext(video_path)[0]+'_compressed.mp4'
        command = 'ffmpeg -y -i "'+video_path+'" -vf scale=' + FINAL_VIDEO_SCALE + " " + compressed_video_path
        logger_video_concat.logger.debug('compress video command: ' + command)
        subprocess.run([command], shell=True)
        if os.path.isfile(compressed_video_path) and os.path.getsize(compressed_video_path) > 0:
            logger_video_concat.logger.debug('видео файл ужат успешно')
            remove_file(video_path)
            logger_video_concat.logger.debug('корипируем сжатый видео файл на место входного')
            copyfile(compressed_video_path, video_path)
        else:
            logger_video_concat.logger.debug('Ошибка сжатия видео для файла: '+ video_path)
            logger_video_concat.logger.debug('Существует ли входящий файл? : '+ str(os.path.isfile(video_path)) )
            if os.path.isfile(video_path):
                logger_video_concat.logger.debug('Размер входящего файла: '+ str(os.path.getsize(video_path)) )
            logger_video_concat.logger.debug('Существует ли выходной файл? : '+ str(os.path.isfile(compressed_video_path)) )
            if os.path.isfile(compressed_video_path):
                logger_video_concat.logger.debug('Размер выходного файла: '+ str(os.path.getsize(compressed_video_path)) )
    finally:
        remove_file(compressed_video_path)


def concat_or_move_video(full_video_path, video_file_path, camera_id, original_file_path):
    try:
        monitoring.set(camera_id, 'concat-segment', video_file_path)
        # Сжимаем видео отрезок
        compress_video(video_file_path)

        if os.path.isfile(full_video_path):
            if os.path.getsize(full_video_path) == 0:
                logger_video_concat.logger.debug("Полный файл пустой! Удалем его и используем отрезак за основу.")
                os.remove(full_video_path)
                logger_video_concat.logger.debug("(Camera #{}): Полный файл НЕ существует. Перемещаем отрезок на место полного файла".format(camera_id))
                move_video(full_video_path, video_file_path)
            else:
                # Объеденяем два видео файла: полный и отрезок
                logger_video_concat.logger.debug("(Camera #{}): Полный файл существует. Объеденяем два видео файла: полный и отрезок".format(camera_id))
                concatenate_videos(full_video_path, video_file_path)
        else:
            logger_video_concat.logger.debug("(Camera #{}): Полный файл НЕ существует. Перемещаем отрезок на место полного файла".format(camera_id))
            move_video(full_video_path, video_file_path)
    finally:
        remove_file(original_file_path)

def enqueue_video_concat(full_video_path, video_file_path, camera_id, original_file_path):
    try:
        q = Queue('concatenate_video_' + os.getenv('WORKER_ID') + '_' + camera_id, connection=redis_conn, default_timeout=86400)
        q.enqueue(
            concat_or_move_video,
            args=(
                full_video_path,
                video_file_path,
                camera_id,
                original_file_path
            ),
            timeout=86400,
            description = f'concatenate_video {camera_id} {os.path.basename(video_file_path)}'
        )
        logger.logger.debug("Событие на склейку видео отправлено в Redis")
    except:
        logger.logger.exception('Ошибка при отправке видео на объеденение. Повтор через 3 сек.')
        time.sleep(3)
        enqueue_video_concat(full_video_path, video_file_path, camera_id, original_file_path)

def save_full_video(model_name, video_file_path, date, camera_id, original_file_path):
    logger.logger.debug("(Camera #{}): Сохранение полного видео: model_name = {}, video_file_path = {}, date = {}, original_file_path = {}".format(
        camera_id,
        model_name,
        video_file_path,
        date.strftime("%d_%m_%Y"),
        original_file_path
    ))
    full_video_path = get_full_video_path(camera_id, model_name, date)
    logger.logger.debug("(Camera #{}): Путь до видео файла с полными данными за день: {}".format(camera_id, full_video_path))
    full_video_directory = "/app/media/full/camera_" + camera_id + "/"
    Path(full_video_directory).mkdir(parents=True, exist_ok=True)
    logger.logger.debug("(Camera #{}): Директрия с полными видео файлами за день: {}".format(camera_id, full_video_directory))

    enqueue_video_concat(full_video_path, video_file_path, camera_id, original_file_path)

def move_video(video_full_path, video_segment_path):
    """
    Переносми первый виедо отрезок за день в основной файл
    """
    try:
        logger_video_concat.logger.debug("Переносим видео из '{}' в '{}'".format(video_segment_path, video_full_path))
        if os.path.isfile(video_segment_path):
            logger_video_concat.logger.debug("Копирование видео файла из '{}' в '{}'".format(video_segment_path, video_full_path))
            copyfile(video_segment_path, video_full_path)
            logger_video_concat.logger.debug("Копирование видео файла завершено из '{}' в '{}'".format(video_segment_path, video_full_path))
    finally:
        remove_file(video_segment_path)


def concatenate_videos(video_full_path, video_segment_path):
    """
    Добавляем видео отрезок в основной видео файл
    """
    try:
        temp_file = str(uuid.uuid4()) + '.mp4'
        cache_dir = "/app/media/temp"
        Path(cache_dir).mkdir(parents=True, exist_ok=True)

        logger_video_concat.logger.debug("Объеденяем видео файлы '{}' c '{}'".format(video_full_path, video_segment_path))
        temp_file_list = "/app/media/temp/filelist_" + str(uuid.uuid4()) + '.txt'
        temp_file_list_file = open(temp_file_list, "w")
        temp_file_list_file.write("file '"+video_full_path+"'\n")
        temp_file_list_file.write("file '"+video_segment_path+"'\n")
        temp_file_list_file.close()

        command = 'ffmpeg -y -f concat -safe 0 -i "'+temp_file_list+'" -c copy "' + cache_dir + os.sep + temp_file + '"'
        logger_video_concat.logger.debug('combine video command: ' + command)
        subprocess.run([command], shell=True)

        logger_video_concat.logger.debug("Копирование видео файла из '{}' в '{}'".format(cache_dir + os.sep + temp_file, video_full_path))
        if not os.path.isfile(cache_dir + os.sep + temp_file):
            return
        copyfile(cache_dir + os.sep + temp_file, video_full_path)
        logger_video_concat.logger.debug("Копирование видео файла завершено из '{}' в '{}'".format(cache_dir + os.sep + temp_file, video_full_path))
    finally:
        remove_file(temp_file_list)
        remove_file(cache_dir + os.sep + temp_file)
        remove_file(video_segment_path)
