import json

def is_json(myjson):
  """Определение JSON"""
  try:
    json.loads(myjson)
  except ValueError as e:
    return False
  return True