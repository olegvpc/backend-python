import os
import subprocess
from include.logger import getLogger

logger = getLogger()

def start_worker():
    """Запускает logger - который не подгружен и по path которого нет :))"""
    logger.logger.debug("Запускаем worker")
    subprocess.run([os.getenv('WORKER_PATH')])
