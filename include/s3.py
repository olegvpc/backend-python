import boto3
from boto3.s3.transfer import TransferConfig


class S3:

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(S3, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.s3 = boto3.resource('s3', endpoint_url='https://s3.yandexcloud.net')

    def print_buckets_list(self):
        for bucket in self.s3.buckets.all():
            print("Bucket name:", bucket.name)

    def list_objects(self, bucket, prefix):
        bucket = self.s3.Bucket(bucket)
        result = []
        objs = bucket.objects.filter(Prefix=prefix)
        for obj in objs:
            result.append(obj)
        return result

    def get_content(self, bucket, key, asString = True):
        obj = self.s3.Object(bucket, key)
        data = obj.get()['Body'].read()
        if (asString):
            return data.decode('utf-8')
        else:
            return data

    def set_content(self, bucket, key, content):
        new_s3_object = self.s3.Object(bucket, key)
        new_s3_object.put(Body=content)

    def upload_file(self, bucket, key, filename):
        s3_client = boto3.client('s3', endpoint_url='https://s3.yandexcloud.net')

        config = TransferConfig(
            multipart_threshold=1024 * 25,
            max_concurrency=10,
            multipart_chunksize=1024 * 25,
            use_threads=True
        )

        s3_client.upload_file(
            filename, bucket, key,
            ExtraArgs={'ContentType': 'video/mp4'},
            Config=config
        )