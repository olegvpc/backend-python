import requests
import os
from include.redis import global_redis_conn
from include.logger import getLogger
from dotenv import load_dotenv

load_dotenv()
logger = getLogger('monitoring')

class Monitoring:
    """Класс выгрузки и корректировки пораметров мониторинга"""
    groups_cameras_map = []

    def get_monitoring_groups_cameras_map(self):
        logger.logger.debug("Запрос для получения groups/cameras map")
        r = requests.get(os.getenv('MAIN_REST_API_URL')+'/monitoring/groups/camers-map', timeout=60, auth=('monitoring', '4hhctmkXcb8LcbgaRMtskrkrYZqbpPgcGqA'))
        response = r.json()
        logger.logger.debug("Получено groups/cameras map: " + str(len(response)))
        return response

    def set(self, camera_id, metric_name, metric_value):
        if (len(self.groups_cameras_map) == 0):
            self.groups_cameras_map = self.get_monitoring_groups_cameras_map()

        group_id = ''
        for map_item in self.groups_cameras_map:
            if map_item['camera'] == camera_id:
                group_id = map_item['group']

        logger.logger.debug("camera_id: " + camera_id)
        logger.logger.debug("group_id: " + group_id)

        if group_id:
            logger.logger.debug("set metric key: " + 'monitoring_' + group_id + '_' + metric_name)
            global_redis_conn.set('monitoring_'+group_id+'_'+metric_name, metric_value)
