import os
import cv2


def get_video_seconds(video_path, camera_fps):
    seconds = 0
    video = False
    if not os.path.isfile(video_path):
        return seconds
    try:
        video = cv2.VideoCapture(video_path)
        total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = int(camera_fps)
        if fps == 0:
            return 0
        seconds = total_frames / fps
    finally:
        if video:
            video.release()
    return seconds


def get_video_frames(video_path):
    frames = 0
    video = False
    if not os.path.isfile(video_path):
        return frames
    try:
        video = cv2.VideoCapture(video_path)
        frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    finally:
        if video:
            video.release()
    return frames


def remove_file(filepath):
    """
    Метод для удаления кеш файлов
    :param filepath:
    :return:
    """
    try:
        if os.path.isfile(filepath):
            os.remove(filepath)
    except:
        pass
