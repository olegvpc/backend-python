import logging
from logging.handlers import TimedRotatingFileHandler
import sys
import traceback
import builtins


class Logger:
    """Класс для Логирования раных типов ошибок"""
    logger: logging.Logger

    def __init__(self, logger_name):
        self.logger = logging.getLogger(logger_name)
        formatter = logging.Formatter(u'%(asctime)s\t%(levelname)s\t%(filename)s:%(lineno)d\t%(message)s')

        logging.basicConfig(level=logging.DEBUG)

        output_file_handler = TimedRotatingFileHandler(
            './logs/' + logger_name + '.log',
            when='D',
            interval=1,
            backupCount=14
        )
        output_file_handler.setFormatter(formatter)

        stderr_handler = logging.StreamHandler(stream=sys.stderr)
        stderr_handler.setFormatter(formatter)
        stderr_handler.setLevel(logging.DEBUG)

        stdout_handler = logging.StreamHandler(stream=sys.stdout)
        stdout_handler.setFormatter(formatter)
        stdout_handler.setLevel(logging.DEBUG)

        self.logger.addHandler(stderr_handler)
        self.logger.addHandler(stdout_handler)
        self.logger.addHandler(output_file_handler)

        logging.captureWarnings(True)
        warnings_logger = logging.getLogger("py.warnings")
        warnings_logger.addHandler(output_file_handler)

    def info(self, msg):
        if msg and "NoneType: None" not in msg:
            self.logger.info(msg)

    def debug(self, msg):
        self.logger.debug(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def exception(self, msg):
        self.logger.exception(msg)


loggers = {}


def getLogger(logger_name='default'):
    global loggers
    if logger_name not in loggers:
        loggers[logger_name] = Logger(logger_name)
    return loggers[logger_name]


def my_except_hook(exctype, value, current_traceback):
    for key in loggers:
        loggers[key].exception("Exception type: {}. Exception value: {}. Traceback: {}".format(exctype, value, traceback.format_exc()))
    sys.__excepthook__(exctype, value, current_traceback)
sys.excepthook = my_except_hook


def my_print_hook(*args, **kwargs):
    for key in loggers:
        log = ''
        for arg in args:
            log += str(arg) + " "
        loggers[key].info(log)

builtins.print = my_print_hook
