from include.s3 import S3
from include.additional_json import is_json
from include.logger import getLogger
from pathlib import Path
import os
import json
import importlib
import subprocess

logger = getLogger()
current_working_dir = Path().absolute()


def download_models():
    logger.logger.debug("Загружаем Models Manifest с Yandex S3")
    s3 = S3()
    models_manifest_string = s3.get_content(os.getenv('YANDEX_S3_BUCKET'), 'models/models.json')
    if not is_json(models_manifest_string):
        logger.logger.debug("Обновление моделей НЕ удалось. Манифест имеет неверный формат.")
        return
    models_manifest = json.loads(models_manifest_string)
    for model_manifest in models_manifest:
        logger.logger.debug("======== Найдена модель для загрузки ========")
        logger.logger.debug("Имя модели: " + model_manifest['name'])
        logger.logger.debug("Путь до модели: " + model_manifest['path'])
        logger.logger.debug("Старт загрузки модели...")
        model_content = s3.get_content(os.getenv('YANDEX_S3_BUCKET'), model_manifest['path'])
        with open('./models/' + model_manifest['name'] + '.py', 'w') as file:
            file.write(model_content)
        folder = model_manifest['folder']
        if folder and not os.path.isdir(str(current_working_dir) + os.sep + folder):
            logger.logger.debug('Загружаем архив с моделью')
            folder_content_zip = s3.get_content(os.getenv('YANDEX_S3_BUCKET'), folder + ".zip", False)
            with open('./' + folder + '.zip', 'wb') as file:
                file.write(folder_content_zip)

            logger.logger.debug('Разархивируем директорию с моделью')
            zip_file = os.path.basename(folder) + '.zip'
            command = "cd models && unzip -o " + zip_file
            subprocess.run([command], shell=True)

            logger.logger.debug('Удаляем архив:' + zip_file)
            os.remove("./models/" + zip_file)
        logger.logger.debug("Модель успешно загружена")


models = {}


def load_models():
    if not models:
        for model_name in importlib.import_module('models').__all__:
            models[model_name] = importlib.import_module('models.' + model_name)
    return models
