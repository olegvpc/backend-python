import os
import json
import time
from include.logger import getLogger
from redis import Redis
from rq import Queue
from include.config import WorkerConfig
from include.process_video_segment import process_video_segment
from dotenv import load_dotenv
from include.redis import redis_conn

load_dotenv()
logger = getLogger('video-recorder')

def add_video_for_analyze(camera_id, filepath, now):
    try:
        config = WorkerConfig()
        cameras_config = config.get_config( os.getenv('WORKER_ID') )

        q = Queue('analyze_video_' + os.getenv('WORKER_ID') + '_' + camera_id, connection=redis_conn, default_timeout=86400)
        q.enqueue(
            process_video_segment,
            args=(
                camera_id,
                filepath,
                now.strftime("%d-%m-%Y %H:%M:%S"),
                cameras_config[camera_id]['models'],
                cameras_config[camera_id]['markups'],
                cameras_config[camera_id]['recording_period']['start'],
                str(cameras_config[camera_id]['fps'])
            ),
            timeout=86400,
            description = f'analyze_video {camera_id} {os.path.basename(filepath)}'
        )
        logger.logger.debug("Событие на обработку видео отправлено в Redis")
    except:
        logger.logger.exception('Ошибка при отправке видео на анализ. Повтор через 3 сек.')
        time.sleep(3)
        add_video_for_analyze(camera_id, filepath, now)
