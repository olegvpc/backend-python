import os
import redis
from dotenv import load_dotenv

load_dotenv()
# определяет с локальной или глобальной БЗ redis будет работать
pool = redis.ConnectionPool(host=os.getenv('REDIS_SERVER'), port=os.getenv('REDIS_PORT'), password=os.getenv('REDIS_PASSWORD'))
redis_conn = redis.Redis(connection_pool=pool)


global_redis_server, global_redis_port, global_redis_password = os.getenv('GLOBAL_REDIS_SERVER'), os.getenv('GLOBAL_REDIS_PORT'), os.getenv('GLOBAL_REDIS_PASSWORD')
# Проверка сделана для обратной совместимости с воркерами, где эти переменные среды не объявлены
if global_redis_server and global_redis_port and global_redis_password:
    global_pool = redis.ConnectionPool(host=global_redis_server, port=global_redis_port, password=global_redis_password)
    global_redis_conn = redis.Redis(connection_pool=global_pool)
else:
    global_redis_conn = redis_conn
