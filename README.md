Cоздать и активировать виртуальное окружение:

python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip

Установить зависимости из файла requirements.txt:

pip install -r requirements.txt