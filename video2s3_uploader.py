from include.s3 import S3
from os import walk
from pathlib import Path
from dotenv import load_dotenv
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
import datetime as dt
import re
import os
import sys


load_dotenv()
MIN_VIDEO_AGE = 1
s3 = S3()


def get_dir_items(path, is_folders = True):
    return next(walk(path), (None, None, []))[1 if is_folders else 2]


def get_files_list_to_upload(root_path):
    files_to_upload = []
    cameras_folders = get_dir_items(root_path, True)
    for cameras_folder in cameras_folders:
        camera_id = cameras_folder[7:]
        print("======= Идентификатор камеры:", camera_id, "=======")

        videos_list = get_dir_items(root_path + cameras_folder, False)
        for video_file in videos_list:
            result = re.findall(r'\d{1,2}_\d{1,2}_\d{4}', video_file)
            date = result[0] if len(result) > 0 else False
            if not date:
                print("Дата видео файла не определена")
                continue
            # Парсинг даты видео файла
            date = dt.datetime.strptime(date, '%d_%m_%Y')
            date = date.date()
            print("Видео файл:", video_file, "Дата видео файла:", date)

            video_age = (dt.datetime.today().date() - date).days
            print("Возрост видео файла (в днях):", video_age)
            if video_age < MIN_VIDEO_AGE:
                print("Файл слшком рано загружать в Yandex S3. Должно пройти минимум (дней):", MIN_VIDEO_AGE)
                continue

            files_to_upload.append({
                "filename": video_file,
                "camera_id": camera_id,
                "date": date,
                "path": root_path + cameras_folder + os.sep + video_file
            })
    return files_to_upload


def upload_video_to_s3(video):
    print("Начало загрузки файла:", video['path'])
    bucket = os.getenv('YANDEX_S3_BUCKET')
    key = "videos/camera_"+video['camera_id']+"/"+video['filename']

    s3.upload_file(bucket, key, video['path'])

    os.remove(video['path'])
    print("Локальная копия файла удалена")


def start_uploading():
    # Старт загрузки видео файлов в Yandex S3
    files_to_upload = get_files_list_to_upload("/app/media/full/")
    for file_to_upload in files_to_upload:
        upload_video_to_s3(file_to_upload)

if len(sys.argv) >= 2 and sys.argv[1] == '--force':
    print('force upload videos to Yandex S3')
    start_uploading()
else:
    print('Scheduler started')
    scheduler = BlockingScheduler()
    scheduler.add_job(
        start_uploading,
        CronTrigger.from_crontab('0 1 * * *')
    )
    scheduler.start()
