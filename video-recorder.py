from video_writer.RTSPRecorderInitializer import RTSPRecorderInitializer
from include.config import WorkerConfig
from dotenv import load_dotenv
from include.logger import getLogger
import os
import json
import gzip
import socket

SOCKET_PATH = '/var/run/videorecorder/sock'
os.makedirs(os.path.dirname(SOCKET_PATH),exist_ok=True)
active_cameras = {}

def generate_config(cameras_dict, worker_config):
    """Формирует config и возвращает json строкой список с объектами из параметров"""
    config = []
    database_config = worker_config.get_config(os.getenv('WORKER_ID'),force=True)
    for camera_id in cameras_dict:
        camera_config_dict = {}
        camera_config_dict['camera_id'] = cameras_dict[camera_id].camera_id
        camera_config_dict['rtsp_link'] = cameras_dict[camera_id].src
        camera_config_dict['fps'] = cameras_dict[camera_id].fps
        camera_config_dict['recording_period'] = {'start':cameras_dict[camera_id].recording_period_start,'end':cameras_dict[camera_id].recording_period_end}
        camera_config_dict['segment_duration'] = cameras_dict[camera_id].segment_duration
        camera_config_dict['models'] = database_config[camera_id]['models']
        camera_config_dict['markups'] = database_config[camera_id]['markups']
        config.append(camera_config_dict)
    return json.dumps(config)

# активация данных окружения os.getenv('..')
load_dotenv()
# создпется экземпляр из класса конфигурации
config = WorkerConfig()
# объект с конфигурацией камеры получен c API https://backend-api.revisorvision.ru/api/workers/get-configurations?worker=worker77
cameras_config = config.get_config( os.getenv('WORKER_ID') )

# Создается экз и файл 'video-recorder.log' для логирования ошибок записи видео
logger = getLogger('video-recorder')

# Запуск записи видео
for key in cameras_config:
    initializer = RTSPRecorderInitializer(
        cameras_config[key]['camera_id'],
        cameras_config[key]['rtsp_link'],
        cameras_config[key]['fps'],
        cameras_config[key]['recording_period']['start'],
        cameras_config[key]['recording_period']['end'],
        cameras_config[key]['segment_duration'],
    )
    active_cameras[cameras_config[key]['camera_id']] = initializer

# тут не понятно что происходит - удаление существующего файла а дальше что-то на низком уровне
if os.path.exists(SOCKET_PATH):
    os.remove(SOCKET_PATH)
control_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
control_socket.bind(SOCKET_PATH)

# тоже не понимаю какая информация в control_socket
while True:
    data, address = control_socket.recvfrom(1024)
    data = data.decode().strip()
    if not data:
        continue
    if data == 'get_config':
        control_socket.sendto(gzip.compress(generate_config(active_cameras, config).encode()),address)
    else:
        try:
            data = json.loads(data)
        except TypeError:
            logger.logger.debug(f'Полученные данные не являются валидным json форматом')
            continue
        if data[-1] == 'restart':
            for camera_to_restart in data[0]:
                if camera_to_restart in active_cameras:
                    logger.logger.debug(f'Камера {camera_to_restart}: Перезапуск записи с обновлением конфигурации')
                    current_camera_config = config.get_config(os.getenv('WORKER_ID'),force=True)[camera_to_restart]
                    active_cameras[camera_to_restart].restart(current_camera_config['camera_id'],
                                                     current_camera_config['rtsp_link'],
                                                     current_camera_config['fps'],
                                                     current_camera_config['recording_period']['start'],
                                                     current_camera_config['recording_period']['end'],
                                                     current_camera_config['segment_duration'])
                else:
                    logger.logger.debug(f'Попытка перезагрузки камеры с id {camera_to_restart}. Нет активной камеры с таким id')
            control_socket.sendto('done'.encode(),address)
